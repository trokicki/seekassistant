﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeekAssistantLibrary;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Description;

namespace SeekAssistantHost {
	class Program {
		static readonly Uri hostUri = new Uri(@"http://localhost:3037/SeekAssistant/");
		static readonly Uri sourceUri = new Uri(@"http://localhost/SeekAssistant/KodeksKarny.html");
		static WebServiceHost webServiceHost;
		
		static void Main(string[] args) {
			int choice;
			DatabaseIndexHelpers.SourceUri = sourceUri;
			//DatabaseIndexHelpers.CheckTagsFuzzySimilarity();
			Console.WriteLine("1. Group keywords using Levenshtein distance algorithm.");
			//Console.WriteLine("2. Check tags similarity based on spelling.");
			Console.WriteLine("2. Start service.");
			choice = Convert.ToInt32(Console.ReadLine());
			//choice = 3;
			switch (choice) {
				case 1:
					float defaultFuzzyLogicLevel = 0.75f;
					Console.WriteLine("Checking fuzzy logic similarity with level {0}...", defaultFuzzyLogicLevel);
					DatabaseIndexHelpers.CheckTagsFuzzySimilarity(defaultFuzzyLogicLevel);
					Console.Write("done.");
					Console.WriteLine();
					Console.ReadKey();
					break;
				case 2:
					ServiceStart();
					ServiceLoop();
					break;
				case 11:
					System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
					Console.WriteLine("Building lexems file");
					stopWatch.Start();
					DatabaseIndexHelpers.BuildLexemsFile();
					Console.WriteLine("Done. Total time: {0}", stopWatch.Elapsed);
					Console.ReadLine();
					break;
			}
		}

		private static void ServiceStart() {
			webServiceHost = new WebServiceHost(typeof(SeekAssistantService), hostUri);
			try {
				WebHttpBinding webHttpBinding = CreateWebHttpBinding();
				webServiceHost.AddServiceEndpoint(typeof(ISeekAssistant), webHttpBinding, "");
				webServiceHost.Open();
				Console.WriteLine("Service started on: {0}", hostUri.ToString());
			} catch (Exception ex) {
				Console.WriteLine("Exception: {0}", ex.Message);
				webServiceHost.Abort();
			}
		}

		private static void ServiceLoop() {
			do {
				Console.WriteLine("Press 'q' to exit");
			} while (Console.ReadKey().KeyChar != 'q');
			webServiceHost.Close();
		}

		private static WebHttpBinding CreateWebHttpBinding() {
			var webHttpBinding = new WebHttpBinding();
			webHttpBinding.CrossDomainScriptAccessEnabled = true;
			return webHttpBinding;
		}
	}
}
