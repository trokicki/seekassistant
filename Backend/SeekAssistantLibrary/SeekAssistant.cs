﻿using HtmlAgilityPack;
using SeekAssistantLibrary.Database;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SeekAssistantLibrary {
	public class SeekAssistant {
		public readonly Uri SourceUri = new Uri(@"http://localhost/SeekAssistant/KodeksKarny.html");
		public readonly CultureInfo culture = new CultureInfo("pl-PL");
		private readonly string articlePattern = @"[Aa]rt\. \d+";
		private readonly string paragraphPattern = @"§\s?\d+\s?((lub|,)\s\d+\s?)*";

		public bool keywordExists(string keywordName) {
			Keyword keyword;
			using (var context = new SeekAssistantDbContext()) {
				keyword = context.Keywords.FirstOrDefault(t => t.Name == keywordName);
			}
			return keyword != null;
		}

		public Keyword addKeyword(string keyword) {
			using (var context = new SeekAssistantDbContext()) {
				Keyword newKeyword = new Keyword() {
					Name = keyword.ToLower()
				};
				context.Keywords.Add(newKeyword);
				Console.WriteLine("Added keyword: {0}", keyword);
				context.SaveChanges();
				return newKeyword;
			}
		}

		public List<EntryWithDependencies> findEntries(string query) {
			using (var context = new SeekAssistantDbContext()) {
				List<Entry> entries = new List<Entry>();
				bool newKeyword = !keywordExists(query);
				if (newKeyword) {
					Keyword addedTag = addKeyword(query);
					entries = parseSourceHtmlFile(addedTag);
				} else {
					Console.WriteLine("Returning stored entries.");
					Keyword keyword = context.Keywords.FirstOrDefault(t => t.Name == query);
					entries = context.Entries.Where(entry => entry.KeywordId == keyword.KeywordId || entry.KeywordId == keyword.ParentId).OrderByDescending(entry => entry.Popularity).ToList();
					keyword.Popularity++;
					context.SaveChanges();
				}
				List<EntryWithDependencies> output = new List<EntryWithDependencies>();
				foreach (var entry in entries) {
					if (!output.Any(e => e.Article.ArticleId == entry.ArticleId)) {
						Article article = context.Articles.Where(art => art.ArticleId == entry.ArticleId).FirstOrDefault();
						output.Add(new EntryWithDependencies() {
							Entry = entry,
							Article = article,
							Paragraphs = context.Paragraphs.Where(p => p.ArticleId == entry.ArticleId).ToList(),
							Dependencies = context.Dependencies.Where(d => d.SourceArticleHtmlNo == article.HtmlArticleNo).Select(d => d.DestinationArticlHtmleNo).ToList()
						});
					}
				}
				return output;
			}
		}

		public List<Entry> parseSourceHtmlFile(Keyword tag) {
			//Console.WriteLine("New tag, forcing lag to pretend something's happening...");
			//System.Threading.Thread.Sleep(10000);
			using (var context = new SeekAssistantDbContext()) {
				List<Entry> output = new List<Entry>();
				HtmlDocument html = getHtmlDocument(SourceUri);
				HtmlNode rootNode = html.GetElementbyId("root");
				string subPartTitle, articleId;
				Console.WriteLine("Parsing data file...");
				foreach (HtmlNode subPartNode in rootNode.SelectNodes("./div[@class='sub-part']")) {
					subPartTitle = subPartNode.SelectSingleNode("./h3").InnerText;
					foreach (HtmlNode articleNode in subPartNode.SelectNodes("./article")) {
						articleId = articleNode.Attributes["id"].Value;
						HtmlNodeCollection paragraphs = articleNode.SelectNodes("./p");
						if (paragraphs != null) {
							for (int paragraphNo = 0; paragraphNo < paragraphs.Count; paragraphNo++) {
								HtmlNode paragraphNode = paragraphs[paragraphNo];
								int contentOffset = culture.CompareInfo.IndexOf(paragraphNode.InnerText, tag.Name, CompareOptions.IgnoreCase);
								if (contentOffset >= 0) {
									Article article = context.Articles.Where(art => art.HtmlArticleNo == articleId).FirstOrDefault();
									if (article == null) {
										article = new Article() {
											HtmlArticleNo = articleId
										};
										context.Articles.Add(article);
										context.SaveChanges();
									}
									Paragraph paragraph = context.Paragraphs.Where(p => p.ArticleId == article.ArticleId && p.HtmlParagraphId == paragraphNo + 1).FirstOrDefault();
									if (paragraph == null) {
										paragraph = new Paragraph() {
											ArticleId = article.ArticleId,
											CachedContent = setNamedEntities(paragraphNode.InnerText),
											HtmlParagraphId = (paragraphNo + 1)
										};
										context.Paragraphs.Add(paragraph);
										context.SaveChanges();
									}
									Entry newEntry = new Entry() {
										KeywordId = tag.KeywordId,
										KeywordOffset = contentOffset,
										ParagraphId = paragraph.ParagraphId,
										ArticleId = article.ArticleId
									};
									output.Add(newEntry);
									context.Entries.Add(newEntry);
									Console.WriteLine("String \"{0}\" found in article {1}, paragraph {2} on position {3}", tag.Name, articleId, paragraphNo + 1, contentOffset);
									string pattern = @"[Aa]rt\.\s*\d+";
									MatchCollection matches = Regex.Matches(paragraphNode.InnerText, pattern);
									if (matches.Count > 0) {
										Console.Write("Dependencies found:");
										foreach (Match match in matches) {
											string htmlArticleId = getArticleId(match.Value);
											Console.Write(htmlArticleId + " | ");
											//Article destinationArticle = context.Articles.Where(art => art.HtmlArticleNo == htmlArticleId).FirstOrDefault();
											context.Dependencies.Add(new Dependency() {
												DestinationArticlHtmleNo = getArticleId(match.Value),
												SourceArticleHtmlNo = articleId,
												SourceParagraphNo = (paragraphNo + 1),
												SourceArticleId = article.ArticleId
											});
										}
										Console.WriteLine();
									}
								}
							} 
						}
					}
				}
				Console.WriteLine("Parsing complete: {0} entries found.", output.Count);
				tag = context.Keywords.Find(tag.KeywordId);
				tag.EntriesAmount = output.Count;
				context.SaveChanges();
				return output;
			}
		}

		public bool VoteUp(int entryId) {
			using (var context = new SeekAssistantDbContext()) {
				Entry entry = context.Entries.Where(e => e.EntryId == entryId).First();
				Console.WriteLine("Entry popularity: " + entry.Popularity);
				entry.Popularity++;
				context.SaveChanges();
			}
			return true;
		}

		private string setNamedEntities(string paragraphContent) {
			string output = setNamedEntities_Articles(paragraphContent);
			output = setNamedEntities_Paragraphs(output);
			return output;
		}

		private string setNamedEntities_Articles(string input) {
			return Regex.Replace(input, articlePattern, delegate(Match match) {
				return String.Format("<enamex type=\"article\">{0}</enamex>", match.ToString());
			});
		}

		private string setNamedEntities_Paragraphs(string input) {
			return Regex.Replace(input, paragraphPattern, delegate(Match match) {
				return String.Format("<enamex type=\"paragraph\">{0}</enamex>", match.ToString());
			});
		}

		private void SetParagraphDependencies(HtmlNode paragraph) {
			MatchCollection matches = Regex.Matches(paragraph.InnerText, articlePattern);
			if (matches.Count > 0) {
				Console.WriteLine("Match in article: {0}", paragraph.ParentNode.Attributes["id"].Value);
				foreach (Match match in matches) {
					Console.WriteLine(match.Value);
				}
			}
		}

		private string getArticleId(string input) {
			return input.Trim().Replace(" ", String.Empty).Replace(".", String.Empty).Replace('a', 'A');
		}

		private HtmlDocument getHtmlDocument(Uri uri) {
			HtmlDocument html = new HtmlDocument();
			WebClient client = new WebClient();
			client.Encoding = Encoding.UTF8;
			html.LoadHtml(client.DownloadString(uri));
			client.Dispose();
			return html;
		}
	}
}
