﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeekAssistantLibrary {
	static class LevenshteinDistance {
		public static int Compute(string left, string right) {
			left = left.ToLower();
			right = right.ToLower();
			int leftLength = left.Length;
			int rightLength = right.Length;
			int[,] distance = new int[leftLength + 1, rightLength + 1];
			if (leftLength == 0) {
				return rightLength;
			}
			if (rightLength == 0) {
				return leftLength;
			}
			for (int i = 0; i <= leftLength; distance[i, 0] = i++) ;
			for (int j = 0; j <= rightLength; distance[0, j] = j++) ;
			for (int i = 1; i <= leftLength; i++) {
				for (int j = 1; j <= rightLength; j++) {
					int cost = (right[j - 1] == left[i - 1]) ? 0 : 1;
					distance[i, j] = Math.Min(
						Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1),
						distance[i - 1, j - 1] + cost);
				}
			}
			return distance[leftLength, rightLength];
		}

		/// <summary>
		/// Compares strings using LevenshteinDistance algorithm and calculates fuzzy logic checking how similar compareTo to basicString is.
		/// </summary>
		/// <param name="compareTo"></param>
		/// <param name="basicString"></param>
		/// <returns></returns>
		public static float ComputeFuzzyLogic(string compare, string basicString) {
			int distance = Compute(compare, basicString);
			int length = Math.Max(compare.Length, basicString.Length);
			return (float)(1.0 - (float)distance / length);
		}
	}
}
