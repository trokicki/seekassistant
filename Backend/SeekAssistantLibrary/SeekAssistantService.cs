﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Web;
using System.Text;
using SeekAssistantLibrary.Database;
using System.Net;

namespace SeekAssistantLibrary {
	public class SeekAssistantService : ISeekAssistant {
		//TODO:
		//1. Levenshtein distance: if no direct hit, compare provided tag with existing ones [fuzzy logic]
		//2. Build index of all words and compare tag with words [very slow - check it in free time]
		//3. 

		private SeekAssistantDbContext context;
		private SeekAssistant seekAssistant = new SeekAssistant();

		[WebGet(ResponseFormat = WebMessageFormat.Json)]
		public List<EntryWithDependencies> Search(string query) {
			Console.WriteLine("Received query: {0}", query); //TODO: NLog
			//string[] tags = query.Split(' ');
			return seekAssistant.findEntries(query);
		}

		[WebGet(ResponseFormat = WebMessageFormat.Json)]
		public bool VoteUp(int EntryId) {
			Console.WriteLine("Voted up: {0}", EntryId);
			seekAssistant.VoteUp(EntryId);
			return true;
		}

		[WebGet(ResponseFormat = WebMessageFormat.Json)]
		public List<Keyword> Tags() {
			using (var context = new SeekAssistantDbContext()) {
				return context.Keywords.OrderByDescending(k => k.Popularity).ThenByDescending(k => k.EntriesAmount).ToList();
			}
		}
	}
}
