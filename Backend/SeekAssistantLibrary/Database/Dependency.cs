﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeekAssistantLibrary.Database {
	public class Dependency {
		public int DependencyId { get; set; }
		public string SourceArticleHtmlNo { get; set; }
		public int SourceParagraphNo { get; set; }
		public string DestinationArticlHtmleNo { get; set; }
		public int? DestinationParagraphNo { get; set; }

		public int SourceArticleId { get; set; }
		public int SourceParagraphId { get; set; }
		public int DestinationArticleId { get; set; }
		public int DestinationParagraphId { get; set; }

	}
}
