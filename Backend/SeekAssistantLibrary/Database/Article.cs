﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeekAssistantLibrary.Database {
	public class Article {
		public int ArticleId { get; set; }
		public string HtmlArticleNo { get; set; }
	}
}
