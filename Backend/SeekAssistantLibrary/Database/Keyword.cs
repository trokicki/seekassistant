﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SeekAssistantLibrary.Database {
	public class Keyword {
		public Keyword() {  }
		public int KeywordId { get; set; }
		public string Name { get; set; }
		public int Popularity { get; set; }
		public int EntriesAmount { get; set; }
		public int ParentId { get; set; }
		public int AssignTrials { get; set; }
		//public virtual List<Entry> Entries { get; set; }
	}
}
