﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeekAssistantLibrary.Database {
	public class SeekAssistantDbContext : DbContext {
		static SeekAssistantDbContext() { }

		public DbSet<Keyword> Keywords { get; set; }
		public DbSet<Entry> Entries { get; set; }
		public DbSet<Dependency> Dependencies { get; set; }
		public DbSet<Article> Articles { get; set; }
		public DbSet<Paragraph> Paragraphs { get; set; }
	}
}
