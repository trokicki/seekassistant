﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeekAssistantLibrary.Database {
	public class Entry {
		public Entry() { }
		public int EntryId { get; set; }
		public int Popularity { get; set; }
		//public string ArticleNo { get; set; }
		//public int ParagraphNo { get; set; }
		[Column(TypeName="ntext")]
		public string Content { get; set; }
		public int KeywordOffset { get; set; }
		public int KeywordId { get; set; }
		public int ArticleId { get; set; }
		public int ParagraphId { get; set; }
		//[ForeignKey("KeywordId")]
		//public virtual Keyword Keyword { get; set; }
		//public virtual List<Dependency> Dependencies {get; set;}
	}
}
