﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeekAssistantLibrary.Database {
	public class Paragraph {
		public int ParagraphId { get; set; }
		public int ArticleId { get; set; }
		public int HtmlParagraphId { get; set; }
		public string CachedContent { get; set; }
	}
}
