﻿using SeekAssistantLibrary.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeekAssistantLibrary {
	public class EntryWithDependencies {
		public Entry Entry { get; set; }
		/// <summary>
		/// List of ArticleIDs
		/// </summary>
		public List<string> Dependencies { get; set; }
		public Article Article { get; set; }
		public List<Paragraph> Paragraphs { get; set; }
	}
}
