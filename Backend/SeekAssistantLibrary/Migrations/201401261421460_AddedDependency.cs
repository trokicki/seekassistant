namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDependency : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dependencies",
                c => new
                    {
                        DependencyID = c.Int(nullable: false, identity: true),
                        SourceEntryId = c.Int(nullable: false),
                        DestinationEntryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DependencyID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Dependencies");
        }
    }
}
