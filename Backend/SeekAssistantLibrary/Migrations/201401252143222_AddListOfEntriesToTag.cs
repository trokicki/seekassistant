namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddListOfEntriesToTag : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Entries",
                c => new
                    {
                        EntryID = c.Int(nullable: false, identity: true),
                        Popularity = c.Int(nullable: false),
                        ArticleExternalID = c.String(),
                        ParagraphExternalID = c.String(),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntryID)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Popularity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entries", "TagID", "dbo.Tags");
            DropIndex("dbo.Entries", new[] { "TagID" });
            DropTable("dbo.Tags");
            DropTable("dbo.Entries");
        }
    }
}
