namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DependencyPlayground : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dependencies", "Entry_EntryID", c => c.Int());
            CreateIndex("dbo.Dependencies", "Entry_EntryID");
            AddForeignKey("dbo.Dependencies", "Entry_EntryID", "dbo.Entries", "EntryID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dependencies", "Entry_EntryID", "dbo.Entries");
            DropIndex("dbo.Dependencies", new[] { "Entry_EntryID" });
            DropColumn("dbo.Dependencies", "Entry_EntryID");
        }
    }
}
