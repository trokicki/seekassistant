namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParagraphTrials7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dependencies", "SourceArticleHtmlNo", c => c.String());
            AddColumn("dbo.Dependencies", "DestinationArticlHtmleNo", c => c.String());
            AddColumn("dbo.Dependencies", "SourceArticleId", c => c.Int(nullable: false));
            AddColumn("dbo.Dependencies", "DestinationArticleId", c => c.Int(nullable: false));
            DropColumn("dbo.Dependencies", "SourceArticleNo");
            DropColumn("dbo.Dependencies", "DestinationArticleNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dependencies", "DestinationArticleNo", c => c.String());
            AddColumn("dbo.Dependencies", "SourceArticleNo", c => c.String());
            DropColumn("dbo.Dependencies", "DestinationArticleId");
            DropColumn("dbo.Dependencies", "SourceArticleId");
            DropColumn("dbo.Dependencies", "DestinationArticlHtmleNo");
            DropColumn("dbo.Dependencies", "SourceArticleHtmlNo");
        }
    }
}
