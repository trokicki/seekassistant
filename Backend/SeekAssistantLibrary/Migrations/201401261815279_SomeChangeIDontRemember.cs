namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeChangeIDontRemember : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Entries", "KeywordId");
            AddForeignKey("dbo.Entries", "KeywordId", "dbo.Keywords", "KeywordId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entries", "KeywordId", "dbo.Keywords");
            DropIndex("dbo.Entries", new[] { "KeywordId" });
        }
    }
}
