namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DependencyPlaygroundRestore : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dependencies", "Entry_EntryID", "dbo.Entries");
            DropIndex("dbo.Dependencies", new[] { "Entry_EntryID" });
            DropColumn("dbo.Dependencies", "Entry_EntryID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dependencies", "Entry_EntryID", c => c.Int());
            CreateIndex("dbo.Dependencies", "Entry_EntryID");
            AddForeignKey("dbo.Dependencies", "Entry_EntryID", "dbo.Entries", "EntryID");
        }
    }
}
