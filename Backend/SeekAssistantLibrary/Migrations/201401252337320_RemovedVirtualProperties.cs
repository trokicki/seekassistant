namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedVirtualProperties : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Entries", "TagID", "dbo.Tags");
            DropIndex("dbo.Entries", new[] { "TagID" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Entries", "TagID");
            AddForeignKey("dbo.Entries", "TagID", "dbo.Tags", "TagID", cascadeDelete: true);
        }
    }
}
