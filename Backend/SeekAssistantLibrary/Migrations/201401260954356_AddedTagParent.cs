namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTagParent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tags", "ParentTagId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tags", "ParentTagId");
        }
    }
}
