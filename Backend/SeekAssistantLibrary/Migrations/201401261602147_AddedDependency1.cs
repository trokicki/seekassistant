namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDependency1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dependencies", "SourceArticleNo", c => c.String());
            AddColumn("dbo.Dependencies", "SourceParagraphNo", c => c.Int(nullable: false));
            AddColumn("dbo.Dependencies", "DestinationArticleNo", c => c.String());
            AddColumn("dbo.Dependencies", "DestinationParagraphNo", c => c.Int(nullable: false));
            AddColumn("dbo.Entries", "ArticleNo", c => c.String());
            AddColumn("dbo.Entries", "ParagraphNo", c => c.Int(nullable: false));
            AddColumn("dbo.Entries", "Content", c => c.String());
            AddColumn("dbo.Entries", "KeywordOffset", c => c.Int(nullable: false));
            DropColumn("dbo.Dependencies", "SourceEntryId");
            DropColumn("dbo.Dependencies", "DestinationEntryId");
            DropColumn("dbo.Entries", "ArticleHtmlId");
            DropColumn("dbo.Entries", "ParagraphHtmlId");
            DropColumn("dbo.Entries", "ParagraphContent");
            DropColumn("dbo.Entries", "ParagraphContentOffset");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Entries", "ParagraphContentOffset", c => c.Int(nullable: false));
            AddColumn("dbo.Entries", "ParagraphContent", c => c.String());
            AddColumn("dbo.Entries", "ParagraphHtmlId", c => c.String());
            AddColumn("dbo.Entries", "ArticleHtmlId", c => c.String());
            AddColumn("dbo.Dependencies", "DestinationEntryId", c => c.Int(nullable: false));
            AddColumn("dbo.Dependencies", "SourceEntryId", c => c.Int(nullable: false));
            DropColumn("dbo.Entries", "KeywordOffset");
            DropColumn("dbo.Entries", "Content");
            DropColumn("dbo.Entries", "ParagraphNo");
            DropColumn("dbo.Entries", "ArticleNo");
            DropColumn("dbo.Dependencies", "DestinationParagraphNo");
            DropColumn("dbo.Dependencies", "DestinationArticleNo");
            DropColumn("dbo.Dependencies", "SourceParagraphNo");
            DropColumn("dbo.Dependencies", "SourceArticleNo");
        }
    }
}
