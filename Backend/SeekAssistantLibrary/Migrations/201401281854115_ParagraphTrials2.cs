namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParagraphTrials2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Entries", "KeywordId", "dbo.Keywords");
            DropIndex("dbo.Entries", new[] { "KeywordId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Entries", "KeywordId");
            AddForeignKey("dbo.Entries", "KeywordId", "dbo.Keywords", "KeywordId", cascadeDelete: true);
        }
    }
}
