namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParagraphTrials5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Entries", "ArticleId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Entries", "ArticleId");
        }
    }
}
