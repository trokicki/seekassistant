namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DependencyNullableDestinationParagraphNo : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Dependencies", "DestinationParagraphNo", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Dependencies", "DestinationParagraphNo", c => c.Int(nullable: false));
        }
    }
}
