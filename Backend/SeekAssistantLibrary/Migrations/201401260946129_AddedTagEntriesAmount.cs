namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTagEntriesAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tags", "EntriesAmount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tags", "EntriesAmount");
        }
    }
}
