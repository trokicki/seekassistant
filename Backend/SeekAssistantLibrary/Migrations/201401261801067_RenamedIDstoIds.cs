namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamedIDstoIds : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Keywords", "ParentId", c => c.Int(nullable: false));
            AlterColumn("dbo.Dependencies", "DependencyId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Entries", "EntryId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Entries", "KeywordId", c => c.Int(nullable: false));
            AlterColumn("dbo.Keywords", "KeywordId", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Dependencies");
            AddPrimaryKey("dbo.Dependencies", "DependencyId");
            DropPrimaryKey("dbo.Entries");
            AddPrimaryKey("dbo.Entries", "EntryId");
            DropPrimaryKey("dbo.Keywords");
            AddPrimaryKey("dbo.Keywords", "KeywordId");
            DropColumn("dbo.Keywords", "ParentTagId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Keywords", "ParentTagId", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.Keywords");
            AddPrimaryKey("dbo.Keywords", "KeywordID");
            DropPrimaryKey("dbo.Entries");
            AddPrimaryKey("dbo.Entries", "EntryID");
            DropPrimaryKey("dbo.Dependencies");
            AddPrimaryKey("dbo.Dependencies", "DependencyID");
            AlterColumn("dbo.Keywords", "KeywordId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Entries", "KeywordId", c => c.Int(nullable: false));
            AlterColumn("dbo.Entries", "EntryId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Dependencies", "DependencyId", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Keywords", "ParentId");
        }
    }
}
