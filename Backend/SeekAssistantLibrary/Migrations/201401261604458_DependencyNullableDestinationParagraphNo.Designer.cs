// <auto-generated />
namespace SeekAssistantLibrary.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.2-21211")]
    public sealed partial class DependencyNullableDestinationParagraphNo : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DependencyNullableDestinationParagraphNo));
        
        string IMigrationMetadata.Id
        {
            get { return "201401261604458_DependencyNullableDestinationParagraphNo"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
