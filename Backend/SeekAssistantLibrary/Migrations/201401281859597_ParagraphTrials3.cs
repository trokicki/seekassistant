namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParagraphTrials3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Paragraphs",
                c => new
                    {
                        ParagraphId = c.Int(nullable: false, identity: true),
                        HtmlArticleNo = c.String(),
                        HtmlParagraphId = c.Int(nullable: false),
                        CachedContent = c.String(),
                    })
                .PrimaryKey(t => t.ParagraphId);
            
            AddColumn("dbo.Dependencies", "SourceParagraphId", c => c.Int(nullable: false));
            AddColumn("dbo.Dependencies", "DestinationParagraphId", c => c.Int(nullable: false));
            AddColumn("dbo.Entries", "ParagraphId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Entries", "ParagraphId");
            DropColumn("dbo.Dependencies", "DestinationParagraphId");
            DropColumn("dbo.Dependencies", "SourceParagraphId");
            DropTable("dbo.Paragraphs");
        }
    }
}
