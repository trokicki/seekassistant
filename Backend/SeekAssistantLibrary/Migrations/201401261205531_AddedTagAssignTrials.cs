namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTagAssignTrials : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tags", "AssignTrials", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tags", "AssignTrials");
        }
    }
}
