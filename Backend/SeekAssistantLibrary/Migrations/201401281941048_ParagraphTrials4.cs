namespace SeekAssistantLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParagraphTrials4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        ArticleId = c.Int(nullable: false, identity: true),
                        HtmlArticleNo = c.String(),
                    })
                .PrimaryKey(t => t.ArticleId);
            
            AddColumn("dbo.Paragraphs", "ArticleId", c => c.Int(nullable: false));
            DropColumn("dbo.Entries", "ArticleNo");
            DropColumn("dbo.Entries", "ParagraphNo");
            DropColumn("dbo.Paragraphs", "HtmlArticleNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Paragraphs", "HtmlArticleNo", c => c.String());
            AddColumn("dbo.Entries", "ParagraphNo", c => c.Int(nullable: false));
            AddColumn("dbo.Entries", "ArticleNo", c => c.String());
            DropColumn("dbo.Paragraphs", "ArticleId");
            DropTable("dbo.Articles");
        }
    }
}
