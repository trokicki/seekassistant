﻿using HtmlAgilityPack;
using SeekAssistantLibrary.Database;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SeekAssistantLibrary {
	public static class DatabaseIndexHelpers {
		public static Uri SourceUri;

		/// <summary>
		/// Iterates through tags with no entries, checks their culture variant spelling and assigns parents.
		/// </summary>
		public static void CheckCultureVariantTagsSpelling() {
			//TODO

		}

		/// <summary>
		/// Iterates through tags with no entries, checks their similarity to not empty tags and assigns parents.
		/// </summary>
		/// <param name="similarityLevel">Indicates minimum fuzzy logic level to say that tags are equal.</param>
		public static void CheckTagsFuzzySimilarity(float similarityLevel = 0.7f) {
			using (var context = new SeekAssistantDbContext()) {
				int maxAssignTrials = 5;
				Keyword[] emptyTags = context.Keywords.Where(t => t.EntriesAmount == 0 && t.AssignTrials <= maxAssignTrials).ToArray();
				Keyword[] notEmptyTags = context.Keywords.Where(t => t.EntriesAmount > 0).ToArray();
				foreach (Keyword emptyTag in emptyTags) {
					foreach (Keyword notEmptyTag in notEmptyTags) {
						Console.Write("Comparing {0} with {1}...", emptyTag.Name, notEmptyTag.Name);
						if (LevenshteinDistance.ComputeFuzzyLogic(emptyTag.Name, notEmptyTag.Name) >= similarityLevel) {
							emptyTag.ParentId = notEmptyTag.KeywordId;
							emptyTag.EntriesAmount = notEmptyTag.EntriesAmount;
							Console.Write("ASSIGNED");
						}
						Console.WriteLine();
						emptyTag.AssignTrials++;
					}
				}
				context.Keywords.RemoveRange(context.Keywords.Where(t => t.AssignTrials > maxAssignTrials));
				context.SaveChanges();
			}
		}

		/// <summary>
		/// Parses source text and grabs all words.
		/// </summary>
		/// <param name="minLength">Minimum word length.</param>
		public static void GrabAllSignificantWords(int minLength = 3) {
			//TODO
			string INPUT = "asdf";
			string[] source = INPUT.Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' }, StringSplitOptions.RemoveEmptyEntries);
			int wordCount = INPUT.Split().Length;
		}

		public static void BuildLexemsFile() {
			buildLexemsFile(@"D:\Projects\SeekAssistant\others\sjp.txt", @"D:\Projects\SeekAssistant\others\lexems.txt");
		}

		private static void buildLexemsFile(string sourceFilePath, string outputFilePath) {
			string line, lexem;
			List<string> strings = new List<string>();
			string[] separators = new string[] { ", " };
			using (StreamReader sourceFileStream = new StreamReader(sourceFilePath)) 
			using(StreamWriter outputFileStream = new StreamWriter(outputFilePath)){
				while ((line = sourceFileStream.ReadLine()) != null) {
					strings = line.Split(separators, StringSplitOptions.RemoveEmptyEntries).ToList();
					lexem = strings.Count > 0 ? lexemFromList(strings) : strings[0];
					outputFileStream.WriteLine(lexem);
				}
			}

		}

		private static string lexemFromList(IList<string> strings) {
			string result = String.Empty;
			for (int i = 0; i < strings.Count - 1; i++) {
				for (int j = i + 1; j < strings.Count; j++) {
					string tmp;
					if (LongestCommonSubstring(strings[i], strings[j], out tmp) > result.Length) {
						result = tmp;
					}
				}
			}
			return result;
		}

		private static int LongestCommonSubstring(string str1, string str2, out string sequence) {
			sequence = string.Empty;
			if (String.IsNullOrEmpty(str1) || String.IsNullOrEmpty(str2))
				return 0;
			int[,] num = new int[str1.Length, str2.Length];
			int maxlen = 0;
			int lastSubsBegin = 0;
			StringBuilder sequenceBuilder = new StringBuilder();
			for (int i = 0; i < str1.Length; i++) {
				for (int j = 0; j < str2.Length; j++) {
					if (str1[i] != str2[j])
						num[i, j] = 0;
					else {
						if ((i == 0) || (j == 0))
							num[i, j] = 1;
						else
							num[i, j] = 1 + num[i - 1, j - 1];
						if (num[i, j] > maxlen) {
							maxlen = num[i, j];
							int thisSubsBegin = i - num[i, j] + 1;
							if (lastSubsBegin == thisSubsBegin) {
								sequenceBuilder.Append(str1[i]);
							} else {
								lastSubsBegin = thisSubsBegin;
								sequenceBuilder.Length = 0;
								sequenceBuilder.Append(str1.Substring(lastSubsBegin, (i + 1) - lastSubsBegin));
							}
						}
					}
				}
			}
			sequence = sequenceBuilder.ToString();
			return maxlen;
		}

		private static HtmlDocument getHtmlDocument(Uri uri) {
			HtmlDocument html = new HtmlDocument();
			WebClient client = new WebClient();
			client.Encoding = Encoding.UTF8;
			html.LoadHtml(client.DownloadString(uri));
			client.Dispose();
			return html;
		}
	}
}
