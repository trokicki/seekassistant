﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SeekAssistantLibrary.Database;

namespace SeekAssistantLibrary {
	[ServiceContract]
	public interface ISeekAssistant {
		[OperationContract]
		List<EntryWithDependencies> Search(string query);
		[OperationContract]
		bool VoteUp(int EntryId);
		[OperationContract]
		List<Keyword> Tags();
	}
}
