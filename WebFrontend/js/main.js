var backendUrl = "http://localhost:3037/SeekAssistant/";

/*
[{"ArticleHtmlId":"Art1","EntryID":0,"ParagraphContent":"§ 2. Nie stanowi przestępstwa czyn zabroniony, którego społeczna szkodliwość jest znikoma.","ParagraphHtmlId":null,"Popularity":0,"Tag":null,"TagID":3},{"ArticleHtmlId":"Art1","EntryID":0,"ParagraphContent":"§ 3. Nie popełnia przestępstwa sprawca czynu zabronionego, jeżeli nie można mu przypisać winy w czasie czynu.","ParagraphHtmlId":null,"Popularity":0,"Tag":null,"TagID":3},{"ArticleHtmlId":"Art2","EntryID":0,"ParagraphContent":"Odpowiedzialności karnej za przestępstwo skutkowe popełnione przez zaniechanie podlega ten tylko, na kim ciążył prawny, szczególny obowiązek zapobiegnięcia skutkowi.","ParagraphHtmlId":null,"Popularity":0,"Tag":null,"TagID":3},{"ArticleHtmlId":"Art4","EntryID":0,"ParagraphContent":"§ 1. Jeżeli w czasie orzekania obowiązuje ustawa inna niż w czasie popełnienia przestępstwa, stosuje się ustawę nową, jednakże należy stosować ustawę obowiązującą poprzednio, jeżeli jest względniejsza dla sprawcy.","ParagraphHtmlId":null,"Popularity":0,"Tag":null,"TagID":3},{"ArticleHtmlId":"Art10","EntryID":0,"ParagraphContent":"§ 3. W wypadku określonym w § 2 orzeczona kara nie może przekroczyć dwóch trzecich górnej granicy ustawowego zagrożenia przewidzianego za przypisane sprawcy przestępstwo; sąd może zastosować także nadzwyczajne złagodzenie kary.","ParagraphHtmlId":null,"Popularity":0,"Tag":null,"TagID":3},{"ArticleHtmlId":"Art11","EntryID":0,"ParagraphContent":"§ 1. Ten sam czyn może stanowić tylko jedno przestępstwo.","ParagraphHtmlId":null,"Popularity":0,"Tag":null,"TagID":3},
{"ArticleHtmlId":"Art11",
"EntryID":0,
"ParagraphContent":"§ 2. Jeżeli czyn wyczerpuje znamiona określone w dwóch albo więcej przepisach ustawy karnej, sąd skazuje za jedno przestępstwo na podstawie wszystkich zbiegających się przepisów.",
"ParagraphHtmlId":null,
"Popularity":0,
"Tag":null,
"TagID":3}]
*/

function getSearchJsonp(query){
	$.ajax({
		type: "GET",
		url: backendUrl + "Search?query=" + query,
		contentType: "application/javascript",
		dataType: "jsonp",
		jsonpCallback: "MethodJsonp",
		success:function(data){
			$("#btnSearch").show();
			$("#searchLoader").hide();
			handleAjaxResults(data);
		},
		error: function(){
			$("#btnSearch").show();
			$("#searchLoader").hide();
			console.error("Something went wrong while getting JSONP");
		}
	});
}

function getTagsJsonp(){
	$.ajax({
		type: "GET",
		url: backendUrl + "Tags",
		contentType: "application/javascript",
		dataType: "jsonp",
		jsonpCallback: "MethodJsonp",
		success:function(data){
			handleAjaxTagsResults(data);
		},
		error: function(){
			console.error("Something went wrong while getting JSONP");
		}
	});
}

function voteUp(entryId){
	$.ajax({
		type: "GET",
		url: backendUrl + "Voteup?EntryId=" + entryId,
		contentType: "application/javascript",
		dataType: "jsonp",
		jsonpCallback: "MethodJsonp",
		success:function(data){
			handleAjaxVoteUpResults(data);
		},
		error: function(){
			console.error("Something went wrong while getting JSONP");
		}
	});
}

function handleAjaxVoteUpResults(data){
	
}

function handleAjaxTagsResults(data){
	var tags = "";
	data.forEach(function(entry){
		tags+= "<li>" + entry.Name + " [popularność: " + entry.Popularity + ", wystąpienia: " + entry.EntriesAmount + "] </li>";
	});
	$("#popular-tags ul").html(tags);
}

function handleAjaxResults(data){
	var resultsHtml = "";
	data.forEach(function(entry){
		resultsHtml += "<li><strong>" + entry.Article.HtmlArticleNo + "</strong> <small>[popularność:" + entry.Entry.Popularity + "]</small></li>";
		resultsHtml += "<ul>";
		entry.Paragraphs.forEach(function(paragraph){
			voteUpUri = backendUrl + "VoteUp?EntryId=" + entry.Entry.EntryId;
			resultsHtml += "<li>" + paragraph.HtmlParagraphId + ". " + paragraph.CachedContent + "</li>";
		});
		resultsHtml += "</ul>";
		if(entry.Dependencies.length > 0)
			resultsHtml += "Znalezione zależności: " + entry.Dependencies.join();
		resultsHtml += "<div><a class=\"voteUpLink\" data-entry=\"" + entry.Entry.EntryId + "\" href=\"#\">tego szukałem</a></div><br/>";
		resultsHtml += "</li>";
	});
	$("#search-results ul").html(resultsHtml);
	styleEnamexes();
	getTagsJsonp();
}

function styleEnamexes(){
	$("enamex[type='article']").css("color", "#0A0");
	$("enamex[type='paragraph']").css("color", "#00A");
}

$(document).ready(function(){
	getTagsJsonp();
	$(document).on("click", ".voteUpLink", function(e){
		e.preventDefault();
		$(this).css("font-weight", "normal");
		voteUp($(this).attr("data-entry"));
	});
	$("#inpQuery").focus();

	$("#search-form").submit(function(e){
		e.preventDefault();
		$("#btnSearch").hide();
		$("#searchLoader").show();
		getSearchJsonp($("#inpQuery").val());
	});

});

