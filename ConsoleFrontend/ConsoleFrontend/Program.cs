﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ConsoleFrontend {
	class Program {
		static SeekAssistantClient seekAssistantClient;
		static Uri hostUri = new Uri("http://localhost:3037/SeekAssistant/Search?query=");

		static void Main(string[] args) {
			Timer timer = new Timer(1000);
			Console.WriteLine("Start: " + DateTime.Now);
			timer.Elapsed += timer_Elapsed;
			timer.Start();


			Console.ReadLine();
			return;
			InitClient();
			MainLoop();
		}

		static void timer_Elapsed(object sender, ElapsedEventArgs e) {
			Console.WriteLine("Timer elapsed");
			
			//(sender as Timer).Close();
		}

		static void InitClient() {
			seekAssistantClient = new SeekAssistantClient(hostUri);
		}

		static void MainLoop() {
			string query;
			string quitString = "/q";
			Console.WriteLine(String.Format("Type in {0} to quit", quitString));
			do {
				Console.Write("[search]> ");
				query = Console.ReadLine();
				if (query != quitString) {
					try {
						string responseContent = seekAssistantClient.Search(query);
						Console.WriteLine(responseContent);
					} catch (Exception ex) {
						Console.WriteLine("Exception occured: {0}", ex.Message);
					}
				}
			} while (query != quitString);
		}

	}
}
