﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleFrontend {
	class SeekAssistantClient {
		string backendUri;
		public SeekAssistantClient(Uri uri) {
			backendUri = uri.ToString();
		}

		public string Search(string query) {
			WebRequest request = WebRequest.Create(backendUri + query);
			request.Credentials = CredentialCache.DefaultCredentials;
			HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
			Stream dataStream = webResponse.GetResponseStream();
			StreamReader streamReader = new StreamReader(dataStream);
			string responseContent = streamReader.ReadToEnd();
			streamReader.Close();
			dataStream.Close();
			webResponse.Close();
			return responseContent;
		}
	}
}
